import json
import requests
import time
import pika
from pika.exceptions import AMQPConnectionError
from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )


# while True:
#     try:
#         parameters = pika.ConnectionParameters(host='rabbitmq')
#         connection = pika.BlockingConnection(parameters)
#         channel = connection.channel()
#         channel.queue_declare(queue='conferences')
#         channel.basic_consume(
#             queue='conferences',
#             on_message_callback=get_conferences,
#             auto_ack=True,
#         )
#         channel.start_consuming()
#     except AMQPConnectionError:
#         print("Could not connect to RabbitMQ")
#         time.sleep(2.0)