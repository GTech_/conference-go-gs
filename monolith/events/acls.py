from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return{"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    params = {
        "q": f'{city},{state},{"US"}',
        "appid": OPEN_WEATHER_API_KEY
        }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except:
        return None
    
    # Create the URL for the current weather API with the latitude
    #   and longitude
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
        }
    url = "https://api.openweathermap.org/data/2.5/weather"
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    try:
        return( 
            {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
            }
        )

    except:
        return None
